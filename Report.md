#**GUI Assessment 1**

## A one column layout 

This makes it more simple to navigate through a interface and gudies people the way you want to.

http://www.goodui.org/#1
## Merging Similar Functions

Having similar things under one tab instaed of trying to fit it under one main tab.

http://www.goodui.org/#3
## Being Direct

Keep it simple and straight forward dont try hide things or hint.

http://www.goodui.org/#10
## Fewer Borders 

Having a few is alright but over doing it can make it crowded.

http://www.goodui.org/#23 

## Vocabulary


## Fewer Form Fields

Having less things to click and fill out

http://www.goodui.org/#13

## Faster Load Times

Optimize code so theres not a long wait to load page.

http://www.goodui.org/#39

## Attention Grabs

Things that need attention from someone made more obvious

http://www.goodui.org/#56

